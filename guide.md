Transform4j 使用指南

-----
### 一. 入门

#### 1.1 基础环境
- jdk1.8+
- spring boot 2.0+

#### 1.2 准备maven依赖

```xml
<dependencies>
    <dependency>
     <groupId>com.itcoon</groupId>
     <artifactId>transform-starter</artifactId>
     <version>1.0.0.RELEASE</version>
    </dependency>
</dependencies>
```
#### 1.3. 确定原类型和目标类型
   原类：student.java

   ```java
    public class Student {

      private Long sid;

      private String name;

      public Long getSid() {
      return sid;
      }

      public void setSid(Long sid) {
      this.sid = sid;
      }

      public String getName() {
      return name;
      }

      public void setName(String name) {
      this.name = name;
      }
    }
   ```

   目标类一：StudentVIO.java
    
   ```java
    public interface StudentVIO {
    
      Long getSid();
    
      String getName();
    
      default Integer getAge(){
          return 18;
      }
    }
   ```

   目标类二：StudentVCO.java
   
   ```java
    public class StudentVCO implements Serializable {
    
        private Long sid;
    
        private String name;
    
        private Integer age;
    
        public Long getSid() {
            return sid;
        }
    
        public void setSid(Long sid) {
            this.sid = sid;
        }
    
        public String getName() {
            return name;
        }
    
        public void setName(String name) {
            this.name = name;
        }
    
        public Integer getAge() {
            return age;
        }
    
        public void setAge(Integer age) {
            this.age = age;
        }
    }
   ```
注意到：
1. 目标类StudentVIO是interface，目标类StudentVCO是class
2. 支持interface的目标类型使用了默认方法

### 二. 编集(ASSENBLE)
编集是Transform4j处理的第一阶段，最大的功能就是拷贝

#### 2.1 直接方式

##### 2.1.1 在目标类定义静态方法中使用

   ```java
    public interface StudentVIO {
    
      Long getId();
    
      String getName();
    
      default Integer getAge(){
          return 18;
      }

      // 加入内容
       static StudentVIO transformFrom(Student student){
          return Transformer
                          .to(StudentVIO.class)
                          .apply(student)
                          .done();
        }
        
    }
   ```
对于StudentVCO同样可以类似处理

##### 2.1.2 在需要转换的地方：

   ```java
    StudentVIO studentVIO = StudentVIO.transformFrom(student);
    // 或者
    StudentVCO studentVCO = StudentVCO.transformFrom(student);
    
    // 或者在业务处理类中直接使用
    StudentVIO studentVIO = Transformer.to(StudentVIO.class).apply(student).done();
   ```

  对于list和page类型的数据，TransForm4j 提供了类似转换的的API
   
   ```java
    // 处理集合
    List<StudentVIO> studentVIOS = Transformer.toList(StudentVIO.class)
    .apply(students)
    .done();
    
    // 处理分页结果
    PageResult<StudentVIO> studentVIOPage = Transformer.toPage(StudentVIO.class)
    .apply(studentPage)
    .done();
   ```


#### 2.2 注解方式

##### 2.2.1 给目标类型标上注解

   改造StudentVIO， StudentVCO如下：
   
   ```java
    @AssembleModel(from = Student.class)
    public interface StudentVIO {
    
      @AssembleModelProperty(mapValue = "sid")
      Long getId();
    
      @AssembleModelProperty
      String getName();
    
      @AssembleModelProperty
      default Integer getAge(){
      return 18;
      }
    
        static StudentVIO transformFrom(Student student){
            return Transformer
                .to(StudentVIO.class)
                .apply(student)
                .done();
        }
    }
    
    //对于class, 同样处理
    @AssembleModel(from = Student.class)
    public class StudentVCO implements Serializable {
    
        @AssembleModelProperty(mapValue = "sid")
        private Long id;
    
        @AssembleModelProperty
        private String name;
    
        private Integer age;
    
        public Long getId() {
            return id;
        }
    
        public void setId(Long id) {
            this.id = id;
        }
    
        public String getName() {
            return name;
        }
    
        public void setName(String name) {
            this.name = name;
        }
    
        public Integer getAge() {
            return age;
        }
    
        public void setAge(Integer age) {
            this.age = age;
        }
        
        public static StudentVCO transformFrom(Student student){
            return Transformer
                .to(StudentVCO.class)
                .apply(student)
                .done();
        }
    }
   ```
 - @AssembleModel：标注该类是transform的目标类型，它将被transform4j扫描解析并加入缓存，from指明原对象
 - @AssembleModelProperty：标注该字段会由原对象的字段转换(assemble), mapValue指明原对象中映射的值，支持级联如`student.course.name`。可用于field和getter方法上
 
 注意，使用注解方式时，只有标注@AssembleModelProperty的字段才会被转换（assemble),但注入（inject，见下文）不受注解影响
 
##### 2.2.2. 配置类上定义扫描配置的注解

   ```java
    @EnableTransform4j(groups = {@AssembleGroup(basePackage = "com.itsherman.transform4j.demo.web.vo")})
    @EnableSwagger2
    @SpringBootApplication
    public class Transform4jDemoApplication {

      public static void main(String[] args) {
      SpringApplication.run(Transform4jDemoApplication.class, args);
      }

    }
   ```
##### 2.2.3. 使用
使用方式同#**2.1.2**

#### 2.3 总结

以上两种方式(2.1,2.2)都可实现对象的拷贝转换。区别有几点：
1. 注解方式会在启动时将目标类型和原类型解析在内存中，而非注解方式更为直接，但第一次转换时花费的时间复杂度相对较高，以后两者没有什么差别；
2. 注解方式使用注解控制目标类转换字段，而直接方式则默认所有字段都做转换，所以注解方式更加灵活
3. 注解方式支持级联映射。假如学生一对一关联了家庭信息表(FamilyInfo)，FamilyInfo有个家庭住址信息(address)，在app的学生列表视图只需要获取家庭住址，而并不需要家庭信息的其他字段。只需要在StudentVIO添加如下
    ```java
    @ApiModelProperty(mapValue="familyInfo.address")
    String getAddress();
    ```
这样就简省省了工作量，没必要只为了一个字段维护多一个FamilyInfoVIO的目标类


**建议**：对于确定的目标类型和原类型，使用第一种方式。只有当原类型不确定时，使用直接方式


### 三. 注入（Injection)

注入是transform4j的第二阶段，在assemble处理完之后执行，它的作用是对特殊值的处理，并可以覆盖assemble的结果，支持lambada表达式

如上StudentVIO的age信息是写死的，但我们不希望这样的结果，而是从业务数据计算而来，我们可以在使用时可以这样子

   ```java
    StudentVIO studentVIO = Transformer.to(StudentVIO.class).aply(student)
    .setAt("age", age->age+1)
    .setAt("name", "孙悟空")
    ...
    .done();`
   ```
上面例子中, 第一个setAt方法第一个参数式目标类的字段名(fieldName), 第二个参数是Function函数式接口（也可以是Integer， Supplier类型）, 作用是将assemble的result数据做二次转换, 上面的结果是age在原来基础上加1，
第二个setAt方法强制赋值给StudentVIO的name属性


这很有用，因为平时有时要做一些不同数据类型的转换，如枚举值和Integer类型。关于这部分，Transform4j高级功能提供了更强大的处理能力，见后面高级部分

由于setAt方法第一个参数是字段名，在业务处理类中直接调用Transformer会带来魔法值的问题，而且开发过程中，字段名容易发生更改。所以建议如上在目标类的静态方法中调用，在目标类中一起维护

#### 四. 高级进阶

##### 4.1 多层嵌套
假如Student类一对多关联一个AttendanceRecord类(考勤记录), 简单业务模型如下：

```java
public class Student {

      private Long id;

      private String name;
      
      private List<AttendanceRecord> attendanceRecords;
      
      //getter,setters...
}

class AttendanceRecord {

    private LocalDateTime startTime;
    
    private LocalDateTime endTime;
}
```
展示层可能需要返回学生的考勤记录列表，先定义展示层VO
```java
public class StudentVO {

      private Long id;

      private String name;
      
      private List<AttendanceRecordVO> attendanceRecords;
      
      //getter,setters...
}

class AttendanceRecordVO {

    private LocalDateTime startTime;
    
    private LocalDateTime endTime;

    //getter,setters...

}
```
使用spring的BeanUtils转换，代码可能如下
```java
 List<AttendanceRecord> records = student.getAttendanceRecords();
 List<AttendanceRecordVO> recordVOs = records.stream().map(record->{
    AttendanceRecordVO recordVO = new AttendanceRecordVO();
    BeanUtils.copyProperties(record, recordVO);
    return recordVO;
 })
 .collect(Collectors.toList());
 
 StudentVO studentVO = new StudentVO();
 BeanUtils.copyProperties(student, studentVO);
 studentVO.setAttendanceRecords(recordVOs);
```

而使用Transform4j,你只需要一行代码足矣

```java
Transformer.to(StudenetVO.class).apply(student).done();
```
上面Student就只嵌套一个对象而已,差别就如此之大，那么如果嵌套两个，三个...呢，没错，transform4j就是这么强大!一行代码就可以完成所有嵌套的转换，而开发人员只需要关注业务

##### 4.2 自定义converter
对于某些特殊的类型，不同的公司的处理方式很可能会不一样（如枚举)。 程序员可以通过实现转换器达到目标。

如下是一个枚举转换器的实现：
```java
public class IntegerToCodeEnumConverter<E extends  Enum<E>&CodeEnum> implements AssembleConverter<Integer, E> {

    @Override
    public boolean support(Type targetType, Object source) {
       return targetType instanceof Class && ((Class<?>)targetType).isEnum() && CodeEnum.class.isAssignableFrom((Class<?>)targetType) && source.getClass().equals(Integer.class);
    }
    
    @Override
    public E convert(Type targetType, Integer attribute){
       Class<CodeEnum> codeEnumClass = (Class<CodeEnum>)targetType;
       CodeEnum[] codeEnums = codeEnumClass.getEnumConstants();
       Optional<CodeEnum> optional = Arrays.stream(codeEnums).filter(codeEnum -> codeEnum.value().equals(attribute)).findAny();
       if(!optional.isPresent()){
           throw new AssembleException(String.format("Illegal CodeEnum Value#%s for %s", attribute, targetType.getTypeName()));
       }
       return (E)optional.get();
    }
}
```
如上Converter实现了从Integer类型转换成enum类型的的处理,开发人员可以根据目标类型和原对象自由定制全局的转换规则。

##### 4.3 增强transformer
 Tranform4j 的入口类是 Transformer, 提供3个入口方法，分别是to(),toList(), toPage()。这三个方法足以应对绝大多数的处理,在某些特殊的情况下。例如某公司的项目规范展示页面分页数据是返回一个自定义的page对象，那么可以参考`com.itcoon.transform.starter.build`报下的源码，自定义实现该包下的Applier，Assembler，轻松达到自由定制的目的。


